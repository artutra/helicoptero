#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glut.h>
#include "image.h"
#include "image.c"

#define PI 3.1415

#define COORD_TEXTURA_PLANO 1.0
#define COORD_TEXTURA_AVIAO 1.0
#define COR_DO_PLANO 0.52,0.52,0.78,1.0
#define COR_DO_AVIAO 0.3,0.52,0.18,1.0
#define TEXTURA_DO_PLANO "f.rgb"
#define TEXTURA_DO_AVIAO "c.rgb"

GLint WIDTH =1024;
GLint HEIGHT=640;

GLfloat obs[3]={0.0,7.0,0.0};
GLfloat look[3]={0.0,3.0,0.0};
GLuint  textura_plano;
GLuint  textura_aviao;

GLshort texturas=1;
GLfloat tetaxz=0;
GLfloat raioxz=14;
GLuint  jato;

GLfloat ctp[4][2]={
  
  {-COORD_TEXTURA_PLANO,-COORD_TEXTURA_PLANO},
  {+COORD_TEXTURA_PLANO,-COORD_TEXTURA_PLANO},
  {+COORD_TEXTURA_PLANO,+COORD_TEXTURA_PLANO},
  {-COORD_TEXTURA_PLANO,+COORD_TEXTURA_PLANO}
  
};

GLfloat cta[4][2]={
        
  {-COORD_TEXTURA_AVIAO,-COORD_TEXTURA_AVIAO},
  {+COORD_TEXTURA_AVIAO,-COORD_TEXTURA_AVIAO},
  {+COORD_TEXTURA_AVIAO,+COORD_TEXTURA_AVIAO},
  {-COORD_TEXTURA_AVIAO,+COORD_TEXTURA_AVIAO}
  
};


void reshape(int width, int height){
  WIDTH=width;
  HEIGHT=height;
  glViewport(0,0,(GLint)width,(GLint)height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(70.0,width/(float)height,0.1,30.0);
  glMatrixMode(GL_MODELVIEW);
}

int helicePrincipal = 0;
int subir = -5;
int praFrente = 0;
int tiroMetralhadora = 0;
int torpedo_a = 0; int torpedo_b = 0;

GLUquadricObj *quadric;

int atirar_m = 0; //atirar metralhadora
int atirar_ta = 0; //atirar torpedo a (esq)
int atirar_tb = 0; //atirar torpedo b (dir)

void compoe_jato(void){  

  GLfloat helice[][3]={    
    {2.0,2.5,1.8},
    {3.0,2.5,0.8},
    {3.2,2.5,1.0},
    {2.2,2.5,2.0},
    {3.2,2.5,3.0},
    {3.0,2.5,3.2},
    {2.0,2.5,2.2},
    {1.0,2.5,3.2},
    {0.8,2.5,3.0},
    {1.8,2.5,2.0},
    {0.8,2.5,1.0},
    {1.0,2.5,0.8}
  };
  
  GLfloat esqui[][3]={        
    {0.0,2.2,1.2},
    {0.0,2.2,1.4},
    {0.0,1.8,1.4},
    {0.0,1.8,1.2},
    {0.0,1.0,1.2},
    {0.0,0.8,1.0},
    {0.0,3.2,1.0},
    {0.0,3.0,1.2}
  };

  GLfloat cauda[][3]={    
    {0.0,0.0,0.0},
    {0.0,1.0,0.0},
    {0.0,1.0,0.75},
    {0.0,0.0,1.0}
  };
  
  /* inicia a composicao do helicoptero */
  jato = glGenLists(1);
  glNewList(jato, GL_COMPILE);
  
  glColor3f(0,0,0.2);
  
  glDisable(GL_TEXTURE_2D);
  
  //ESQUI
  //lado a
  glPushMatrix();
  glTranslatef(-.4,-2+(subir*.2),6+(praFrente*.2));
  glRotatef(270,1,0,0);
  glBegin(GL_POLYGON); 
  glVertex3fv(esqui[0]);
  glVertex3fv(esqui[1]);
  glVertex3fv(esqui[2]);
  glVertex3fv(esqui[3]);
  glVertex3fv(esqui[4]);
  glVertex3fv(esqui[5]);
  glVertex3fv(esqui[6]);
  glVertex3fv(esqui[7]);
  glEnd();
  glPopMatrix();
  //lado b
  glPushMatrix();
  glTranslatef(.4,-2+(subir*.2),6+(praFrente*.2));
  glRotatef(270,1,0,0);
  glBegin(GL_POLYGON); 
  glVertex3fv(esqui[0]);
  glVertex3fv(esqui[1]);
  glVertex3fv(esqui[2]);
  glVertex3fv(esqui[3]);
  glVertex3fv(esqui[4]);
  glVertex3fv(esqui[5]);
  glVertex3fv(esqui[6]);
  glVertex3fv(esqui[7]);
  glEnd();
  glPopMatrix();    
  
  //HELICE PRINCIPAL
  //helice
  glColor3f(0.6,0,0.2);
  glPushMatrix();
  glTranslatef(0,0,4+(praFrente*.2));
  glRotatef((GLfloat)helicePrincipal*32,0,1,0);
  glTranslatef(-3.6,-.5+(subir*.2),-3.6);
  glScalef(1.8,1,1.8);
  glBegin(GL_POLYGON); 
  glVertex3fv(helice[0]);
  glVertex3fv(helice[1]);
  glVertex3fv(helice[2]);
  glVertex3fv(helice[3]);
  glVertex3fv(helice[4]);
  glVertex3fv(helice[5]);
  glVertex3fv(helice[6]);
  glVertex3fv(helice[7]);
  glVertex3fv(helice[8]);
  glVertex3fv(helice[9]);
  glVertex3fv(helice[10]);
  glVertex3fv(helice[11]);
  glEnd();
  glPopMatrix();
  //suporte
   glColor3f(0,0,0.2);
  glPushMatrix();
  glTranslatef(0,2+(subir*.2),4+(praFrente*.2));
  glRotatef(90,1,0,0);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.15, 0.15, 2.5, 12, 3);
  glPopMatrix();  
  
  //HELICE SENCUNDARIA
  glColor3f(0.6,0,0.2);
  glPushMatrix();
  glTranslatef(1.2,-.8+(subir*.2),-.4+(praFrente*.2));
  glScalef(.4,.4,.4);
  glRotatef(90,0,0,1);
  //glRotatef((GLfloat)helicePrincipal*16,0,1,0);
  glBegin(GL_POLYGON); 
  glVertex3fv(helice[0]);
  glVertex3fv(helice[1]);
  glVertex3fv(helice[2]);
  glVertex3fv(helice[3]);
  glVertex3fv(helice[4]);
  glVertex3fv(helice[5]);
  glVertex3fv(helice[6]);
  glVertex3fv(helice[7]);
  glVertex3fv(helice[8]);
  glVertex3fv(helice[9]);
  glVertex3fv(helice[10]);
  glVertex3fv(helice[11]);
  glEnd();
  glPopMatrix();
  
  glEnable(GL_TEXTURE_2D);  
  
  // CAUDA  
  glColor3f(0,0,0.2);
  glPushMatrix();
  glTranslatef(0,0+(subir*.2),0+(praFrente*.2));
  //barra
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.15, 0.15, 4, 12, 3);
  //cauda
  glBegin(GL_POLYGON); 
  glTexCoord2fv(cta[0]); glVertex3fv(cauda[0]);
  glTexCoord2fv(cta[1]); glVertex3fv(cauda[1]);
  glTexCoord2fv(cta[2]); glVertex3fv(cauda[2]);
  glTexCoord2fv(cta[3]); glVertex3fv(cauda[3]);
  glEnd();
  glPopMatrix();    

  // CABINE PILOTO  
  //cabine completa
  glTranslatef(0,0.3,3.5);
  glPushMatrix();
  glTranslatef(0,0+(subir*.2),0+(praFrente*.2));
  quadric=gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluSphere(quadric,1,12,12);
  glPopMatrix();
  //
  glTranslatef(0,0,1.2);
  glPushMatrix();
  glTranslatef(0,0+(subir*.2),0+(praFrente*.2));
  quadric=gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluSphere(quadric,1,12,12);
  glPopMatrix();
  //
  glTranslatef(0,0+(subir*.2),-1.2+(praFrente*.2));
  glPushMatrix();
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 1, 1, 1.2, 12, 3);
  glPopMatrix();  
  
  glDisable(GL_TEXTURE_2D);
  //cabine piloto
  glColor3f(0.8,0.6,0.6);
  glPushMatrix();
  glTranslatef(0,0,1.3);
  quadric=gluNewQuadric();
  gluSphere(quadric,1,12,8);
  glPopMatrix();  
    
  //METRALHADORAS
  //lado a
  glColor3f(.8,.6,.2);
  glPushMatrix();
  glTranslatef(1,.2,0);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.08, 0.08, .8, 12, 3);
  glPopMatrix();
  glColor3f(.6,.4,.2);
  glPushMatrix();
  glTranslatef(1,.2,0);
  glScalef(1,1,1.2);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.06, 0.06, .8, 12, 3);
  glPopMatrix();  
  //lado b
  glColor3f(.8,.6,.2);
  glPushMatrix();
  glTranslatef(-1,.2,0);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.08, 0.08, .8, 12, 3);
  glPopMatrix();
  glColor3f(.6,.4,.2);
  glPushMatrix();
  glTranslatef(-1,.2,0);
  glScalef(1,1,1.2);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.06, 0.06, .8, 12, 3);
  glPopMatrix();  
  
  // BARRA SUPORTE CANHOES  
  glColor3f(.8,.6,.2);
  glPushMatrix();
  glTranslatef(-1.4,-.2,0);
  glRotatef(90,0,1,0);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.15, 0.15, 2.8, 12, 3);
  glPopMatrix();  
  
  // CANHOES
  //lado a
  glColor3f(.8,.6,.2);
  glPushMatrix();
  glTranslatef(1.3,-.2,-.2);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.18, 0.18, .8, 12, 3);
  glPopMatrix();
  glColor3f(.6,.4,.2);
  glPushMatrix();
  glTranslatef(1.3,-.2,-.2);
  glScalef(1,1,1.2);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.10, 0.10, .8, 12, 3);
  glPopMatrix();
  //lado b
  glColor3f(.8,.6,.2);
  glPushMatrix();
  glTranslatef(-1.3,-.2,-.2);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.18, 0.18, .8, 12, 3);
  glPopMatrix();
  glColor3f(.6,.4,.2);
  glPushMatrix();
  glTranslatef(-1.3,-.2,-.2);
  glScalef(1,1,1.2);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.10, 0.10, .8, 12, 3);
  glPopMatrix();
  
  //BALAS METRALHADORAS
  glColor3f(255/255.0f,0/255.0f,0/255.0f);
  //lado a
  glPushMatrix();
  glScalef(.08,.04,.24);
  glTranslatef(12*atirar_m,4,5+tiroMetralhadora*8);
  quadric=gluNewQuadric();
  gluSphere(quadric,1,12,8);
  glPopMatrix();
  //lado b  
  glPushMatrix();
  glScalef(.08,.04,.24);
  glTranslatef(-12,4,1+tiroMetralhadora*8);
  quadric=gluNewQuadric();
  gluSphere(quadric,1,12,8);
  glPopMatrix();
  
  //TORPEDOS
  //lado a
  glColor3f(0.8,0,0);
  glPushMatrix();
  glScalef(.2,.2,.9);
  glTranslatef(6.5,-1,.2+torpedo_a);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.4, 0.4, .8, 12, 3);
  glPopMatrix();
  //
  glColor3f(255/255.0f,0/255.0f,0/255.0f);  
  glPushMatrix();
  glTranslatef(1.4,-.2,.8+torpedo_a*.9);
  glScalef(.19,.19,.19);
  glTranslatef(-.5,0,0);
  glutSolidCone(.5, 2, 50, 50);
  glPopMatrix();
  //lado b
  glColor3f(0.8,0,0);
  glPushMatrix();
  glScalef(.2,.2,.9);
  glTranslatef(-6.5,-1,.2+torpedo_b);
  quadric = gluNewQuadric();
  gluQuadricTexture(quadric, GL_TRUE);
  gluCylinder(quadric, 0.4, 0.4, .8, 12, 3);
  glPopMatrix();
  //
  glColor3f(255/255.0f,0/255.0f,0/255.0f);  
  glPushMatrix();
  glTranslatef(-1.4,-.2,.8+torpedo_b*.9);
  glScalef(.19,.19,.19);
  glTranslatef(.5,0,0);
  glutSolidCone(.5, 2, 50, 50);
  glPopMatrix();
    
  /* termina a composicao do helicoptero*/
  glEndList();
}

void display(void){
     
  compoe_jato();
     
  glEnable(GL_DEPTH_TEST);
  
  glDepthMask(GL_TRUE);
  glClearColor(.8,.8,1.0,1.0);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  
  glPushMatrix();

  /* calcula a posicao do observador */
  obs[0]=raioxz*cos(2*PI*tetaxz/360);
  obs[2]=raioxz*sin(2*PI*tetaxz/360);
  gluLookAt(obs[0],obs[1],obs[2],look[0],look[1],look[2],0.0,1.0,0.0);

  /* habilita/desabilita uso de texturas*/
  if(texturas){
    glEnable(GL_TEXTURE_2D);  
  }
  else{
    glDisable(GL_TEXTURE_2D);
  }

  glColor4f(COR_DO_PLANO);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
  glBindTexture(GL_TEXTURE_2D,textura_plano);
  
  glBegin(GL_QUADS);
  glTexCoord2fv(ctp[0]);  glVertex3f(-10,0,10);
  glTexCoord2fv(ctp[1]);  glVertex3f(10,0,10);
  glTexCoord2fv(ctp[2]);  glVertex3f(10,0,-10);
  glTexCoord2fv(ctp[3]);  glVertex3f(-10,0,-10);
  glEnd();
  glTranslatef(0.0,2.0,-3.0);

  glColor4f(COR_DO_AVIAO);
  glBindTexture(GL_TEXTURE_2D,textura_aviao);
  glCallList(jato);

  glPopMatrix();
  glutSwapBuffers();
}

int helices_enabled = 0;
void special(int key, int x, int y){
  switch (key) {
  case GLUT_KEY_UP:
    if(helices_enabled==1 && subir<20){subir++;}
    //obs[1]=obs[1]+1;
    glutPostRedisplay();
    break;
  case GLUT_KEY_DOWN:
    if(helices_enabled==1 && subir>-5){subir--;}
    //obs[1] =obs[1]-1;
    glutPostRedisplay();
    break;
  case GLUT_KEY_LEFT:
    if(helices_enabled==1 && subir>=-4){praFrente++;}
    else{tetaxz=tetaxz+2;}
    glutPostRedisplay();
    break;
  case GLUT_KEY_RIGHT:
    if(helices_enabled==1 && subir>=-4){praFrente--;}
    else{tetaxz=tetaxz-2;}
    glutPostRedisplay();
    break;
  }
}

int fps = 30;

void refresh(int value){
     if(helicePrincipal<16 && helices_enabled){helicePrincipal++;} else{helicePrincipal=0;}
     if(tiroMetralhadora<10 && atirar_m){tiroMetralhadora++;}else{tiroMetralhadora=0;}     
     if(torpedo_a<40 && atirar_ta){torpedo_a++;}else{torpedo_a=0; atirar_ta=0;}
     if(torpedo_b<40 && atirar_tb){torpedo_b++;}else{torpedo_b=0; atirar_tb=0;}
     
     glutTimerFunc( 1000/fps,refresh, 0);
     glutPostRedisplay();  
}

void keyboard(unsigned char key, int x, int y){
  switch (key) {
  case 27:
    exit(0);
    break;
  case 'i':
    if(helices_enabled==0) helices_enabled = 1;
    break;
  case 'I':
    if(subir==-5){helices_enabled = 0;}
    break;
  case 'm':
    if(atirar_m==0) atirar_m = 1;
    break;
  case 'M':
    tiroMetralhadora=0;
    atirar_m = 0;
    break;  
  case 't':
    if(atirar_ta==0)atirar_ta = 1;
    break;
  case 'T':
    if(atirar_tb==0)atirar_tb = 1;
    break;
  case 'r':
    raioxz=raioxz+1;
    glutPostRedisplay();
    break;
  case 'R':
    raioxz=raioxz-1;
    if(raioxz==0){
      raioxz=1;
    }
    glutPostRedisplay();
    break;
  }
}

void carregar_texturas(void){
  IMAGE *img;
  GLenum gluerr;

  /* textura do plano */
  glGenTextures(1, &textura_plano);
  glBindTexture(GL_TEXTURE_2D, textura_plano);
  
  if(!(img=ImageLoad(TEXTURA_DO_PLANO))) {
    fprintf(stderr,"Error reading a texture.\n");
    exit(-1);
  }

  gluerr=gluBuild2DMipmaps(GL_TEXTURE_2D, 3, 
			   img->sizeX, img->sizeY, 
			   GL_RGB, GL_UNSIGNED_BYTE, 
			   (GLvoid *)(img->data));
  if(gluerr){
    fprintf(stderr,"GLULib%s\n",gluErrorString(gluerr));
    exit(-1);
  }

  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);

  /* textura do aviao */
  glGenTextures(1, &textura_aviao);
  glBindTexture(GL_TEXTURE_2D, textura_aviao);

  
  if(!(img=ImageLoad(TEXTURA_DO_AVIAO))) {
    fprintf(stderr,"Error reading a texture.\n");
    exit(-1);
  }

  gluerr=gluBuild2DMipmaps(GL_TEXTURE_2D, 3, 
			   img->sizeX, img->sizeY, 
			   GL_RGB, GL_UNSIGNED_BYTE, 
			   (GLvoid *)(img->data));
  if(gluerr){
    fprintf(stderr,"GLULib%s\n",gluErrorString(gluerr));
    exit(-1);
  }

  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
  glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
  
}

void init(){
  carregar_texturas();
  compoe_jato();
  glShadeModel(GL_FLAT);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_TEXTURE_2D);
}

int main(int argc,char **argv){
  glutInitWindowPosition(0,0);
  glutInitWindowSize(WIDTH,HEIGHT);
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_RGB|GLUT_DEPTH|GLUT_DOUBLE);

  if(!glutCreateWindow("Helicóptero")) {
    fprintf(stderr,"Error opening a window.\n");
    exit(-1);
  }

  init();
  
  glutKeyboardFunc(keyboard);
  glutSpecialFunc(special);
  glutDisplayFunc(display);
  glutTimerFunc(1000/fps,refresh,0);
  glutReshapeFunc(reshape);
  
  glutMainLoop();

  return(0);
}
